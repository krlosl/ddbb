-- Comentarios del usuario 36 sobre la foto 12 del usuario 11
use instagram_low_cost;
SELECT c.comentario
FROM comentarios c
JOIN comentariosFotos cf ON c.idComentario = cf.idComentario
JOIN usuarios u ON c.idUsuario = u.idUsuario
WHERE u.idUsuario = 36 AND cf.idFoto = 12;
